from logs_model.nodes import *
from logs_model.utils import log_message
from logs_model import mathutils


class Event(object):
    def __init__(self, start_time, end_time):
        assert start_time >= 0
        assert end_time >= 0
        assert start_time <= end_time

        self.start_time = start_time
        self.end_time = end_time

    def duration(self):
        return self.end_time - self.start_time

    def handle(self, field):
        raise NotImplementedError

    def __lt__(self, other):
        return self.end_time < other.end_time

    def __eq__(self, other):
        return self.end_time == other.end_time


class MoveEvent(Event):
    def __init__(self, field, curr_time, node):
        assert isinstance(node, MovingNode)
        self.node = node

        next_pos = mathutils.random_point_in_circle(node.prev_pos, node.move_radius)
        while not field.contains(next_pos):
            next_pos = mathutils.random_point_in_circle(node.prev_pos, node.move_radius)
        node.next_pos = next_pos    # TODO: нужно ли это хранить в ноде?
        node.start_move_time = curr_time

        super().__init__(curr_time, curr_time + node.move_time)

    def handle(self, field):
        """
        Handles MoveEvent:
            1) Updates node state
            2) generates new move event for node
            3) generates new transfer events
        """
        assert self.node.id in field.nodes
        log_message("Moved N%d from (%d,%d) to (%d,%d); dist=%d" %
                    (self.node.id, self.node.prev_pos.x, self.node.prev_pos.y,
                     self.node.next_pos.x, self.node.next_pos.y,
                     mathutils.distance(self.node.prev_pos, self.node.next_pos)), self.end_time)

        self.node.handle_move_event()

        if self.node.should_break():
            return [NodeBreakEvent(self.node, self.end_time)]

        events = [MoveEvent(field, self.end_time, self.node)]

        events += gen_new_transfer_events_for_node(self.node, field.nodes, self.end_time)
        return events


class NodeBreakEvent(Event):
    def __init__(self, node, break_time):
        super().__init__(break_time, break_time)
        self.node = node

        self.field = None

    def handle(self, field):
        assert self.node.id in field.nodes

        self.field = field  # needed to handle event in statistics

        events = []
        # create instant disconnect events for every node
        # It could be done because transfer events are generated only in MoveEvent
        # after check for node breaking
        for tm, conn in self.node.transfer_module.connections.items():
            if conn[0]:  # disconnect if connected
                events += [TransferEndEvent(tm, self.node.transfer_module, self.end_time, self.end_time)]
        field.break_node(self.node.id, self.end_time)
        log_message("N%d have broken; Data:" % self.node.id, self.end_time, "red")
        log_message(str(self.node.strategy))
        return events


class GenerateDataEvent(Event):
    def __init__(self, curr_time, node):
        assert isinstance(node, MovingNode)

        self.node = node
        t = self.node.get_next_data_gen_time()
        super().__init__(curr_time, curr_time + t)

    def handle(self, field):
        # stop generation if node removed from field
        if self.node.id not in field.nodes:
            return []

        res = []
        chunk, events = self.node.handle_generate_data_event(self.end_time)
        res += events
        res += [GenerateDataEvent(self.end_time, self.node)]
        log_message("N%d generated data chunk: %s" % (self.node.id, chunk), self.end_time)
        return res


class TransferStartEvent(Event):
    def __init__(self, tm1, tm2, curr_time, event_time):
        super().__init__(curr_time, event_time)
        self.tm1 = tm1
        self.tm2 = tm2

    def handle(self, field):
        """
        Handles start transfer event:

        """
        assert self.tm1.owner.id in field.nodes
        assert self.tm2.owner.id in field.nodes

        log_message("Start transfer between N%d and N%d" %
                    (self.tm1.owner.id, self.tm2.owner.id), self.end_time, "green")
        events = []
        events += self.tm1.handle_connection_event(self.tm2, self.end_time)
        events += self.tm2.handle_connection_event(self.tm1, self.end_time)
        return events


class TransferEndEvent(Event):
    def __init__(self, tm1, tm2, curr_time, event_time):
        super().__init__(curr_time, event_time)
        self.tm1 = tm1
        self.tm2 = tm2

    def handle(self, field):
        """
        Handles end transfer event:

        """
        log_message("End transfer between N%d and N%d" %
                    (self.tm1.owner.id, self.tm2.owner.id), self.end_time, "purple")
        events = []
        e1 = self.tm1.handle_disconnection_event(self.tm2, self.end_time)
        e2 = self.tm2.handle_disconnection_event(self.tm1, self.end_time)

        # Add new events only if node exist at field
        if self.tm1.owner.id in field.nodes:
            events += e1
        if self.tm2.owner.id in field.nodes:
            events += e2
        return events


class ChunkTransferEvent(Event):
    def __init__(self, tm1, tm2, connection_id, chunk, curr_time, duration, chunk_owner=None):
        super().__init__(curr_time, curr_time + duration)
        self.tm1 = tm1
        self.tm2 = tm2
        self.chunk = chunk
        self.chunk_owner = chunk_owner
        self.connection_id = connection_id

        assert not self.tm1.connections[self.tm2][2]

    def __repr__(self):
        return "<%s to %s, c: (%s:%s), cid: %s>" %\
               (self.tm1.owner.id, self.tm2.owner.id, self.chunk_owner.id, self.chunk, self.connection_id)

    def handle(self, field):
        if not self.tm1.is_connection_up(self.tm2, self.connection_id):
            return []
        if not self.tm1.connections[self.tm2][2]:   # means connection was canceled
            return []

        # if one of nodes was deleted during transfer connection should be canceled already
        assert self.tm1.owner.id in field.nodes
        assert self.tm2.owner.id in field.nodes

        assert self.chunk_owner is not None
        chunk_info = "(%s: %s)" % (self.chunk_owner.id, self.chunk)
        log_message(("Transferred chunk from N%d to N%d; cid: %s; " + chunk_info) %
                    (self.tm1.owner.id, self.tm2.owner.id, self.connection_id), self.end_time, "blue")

        events = []
        events += self.tm2.handle_receive_data_chunk(self.tm1, self.end_time, self.chunk, self.chunk_owner)
        events += self.tm1.handle_transfer_data_chunk(self.tm2, self.end_time, self.chunk, self.chunk_owner)
        log_message(str(self.tm1.owner.strategy))
        log_message(str(self.tm2.owner.strategy))
        return events


def gen_new_transfer_events_for_node(node, nodes, curr_time):
        events = []
        for _, n in nodes.items():
            if n == node:
                continue
            events += gen_new_transfer_events(node, n, curr_time)
        return events


def gen_new_transfer_events(n1, n2, curr_time):
    v1 = n1.velocity
    d1 = mathutils.Disk(n1.curr_pos(curr_time), n1.transfer_module.link_radius)
    v2 = n2.velocity
    d2 = mathutils.Disk(n2.curr_pos(curr_time), n2.transfer_module.link_radius)

    res = []
    # get times of events
    times = mathutils.calc_disks_overlapping_times(d1, v1, d2, v2)
    if times is None:   # transfer state will not be changed
        return res

    t1, t2 = times
    if t1 < 0 and t2 < 0:
        return res
    if t1 == t2:
        return res

    min_t = min(n1.end_move_time, n2.end_move_time)
    if t1 <= 0:
        if curr_time + t2 < min_t:
            res += [TransferEndEvent(n1.transfer_module, n2.transfer_module, curr_time, curr_time + t2)]
        return res

    if curr_time + t1 <= min_t:
        res += [TransferStartEvent(n1.transfer_module, n2.transfer_module, curr_time, curr_time + t1)]
    if curr_time + t2 < min_t:
        res += [TransferEndEvent(n1.transfer_module, n2.transfer_module, curr_time, curr_time + t2)]
    return res


def gen_startup_transfer_events(node, nodes, curr_time):
    d1 = mathutils.Disk(node.curr_pos(curr_time), node.transfer_module.link_radius)
    res = []
    for _, n in nodes.items():
        if n == node:
            continue
        d2 = mathutils.Disk(n.curr_pos(curr_time), n.transfer_module.link_radius)
        if mathutils.is_overlapping(d1, d2):
            res += [TransferStartEvent(node.transfer_module, n.transfer_module, curr_time, curr_time)]
    return res
