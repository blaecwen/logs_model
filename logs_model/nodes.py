import numpy as np
from random import random

from logs_model import data
from logs_model import mathutils


# TODO: combine Node and MovingNode. THEY ARE THE SAME!
class Node(object):
    def __init__(self, strategy="simple"):
        from logs_model.transfer_modules import TransferModule
        from logs_model.strategy import SimpleServerStrategy, SmartServerStrategy
        self.id = 0

        self.prev_pos = None

        if strategy == "simple":
            self.strategy = SimpleServerStrategy()
        elif strategy == "smart":
            self.strategy = SmartServerStrategy()
        else:
            assert False
        self.strategy.owner = self

        self.transfer_module = TransferModule(10000, 100)
        self.transfer_module.owner = self

    def is_server_node(self) -> bool:
        return self.strategy.is_server()

    def handle_generate_data_event(self, curr_time):
        return None, []
        # chunk = self._data_generator.generate_data_chunk(curr_time)
        # return chunk, self.strategy.handle_generate_data_event(chunk, curr_time)

    def handle_connection_event(self, node, curr_time):
        return self.strategy.handle_connection_event(node, curr_time)

    def handle_disconnection_event(self, node, curr_time):
        return self.strategy.handle_disconnection_event(node, curr_time)

    def handle_transfer_data_chunk(self, node, curr_time, chunk, chunk_owner):
        return self.strategy.handle_transfer_data_chunk(node, curr_time, chunk, chunk_owner)

    def handle_receive_data_chunk(self, node, curr_time, chunk, chunk_owner):
        return self.strategy.handle_receive_data_chunk(node, curr_time, chunk, chunk_owner)

    def curr_pos(self, curr_time):
        return self.prev_pos.to_np_array()

    @property
    def velocity(self):
        return np.array([0, 0])

    @property
    def end_move_time(self):
        return float("inf")

    def __repr__(self):
        return "".join(["Node: lr: ", str(self.transfer_module.link_radius)])


class MovingNode(object):
    def __init__(self, speed, move_radius, strategy="simple"):
        from logs_model.transfer_modules import TransferModule
        from logs_model.strategy import SimpleStrategy, SmartStrategy

        self.id = 0
        self.prev_pos = None

        self.speed = speed
        self.move_radius = move_radius
        self.next_pos = None
        self.start_move_time = None

        self.break_probability = 0.001

        if strategy == "simple":
            self.strategy = SimpleStrategy()
        elif strategy == "smart":
            self.strategy = SmartStrategy()
        else:
            assert False
        self.strategy.owner = self

        self.transfer_module = TransferModule(100, 30)
        self.transfer_module.owner = self

        self._data_generator = data.DataGenerator(1, 0.5, 10, 5)

    def is_server_node(self) -> bool:
        return self.strategy.is_server()

    def should_break(self) -> bool:
        """
        To break or not to break, that is the question!
        """
        return random() <= self.break_probability

    def curr_pos(self, curr_time):
        if curr_time == self.start_move_time:
            return self.prev_pos.to_np_array()
        return self.prev_pos.to_np_array() + self.velocity * (curr_time - self.start_move_time)

    def handle_move_event(self):
        self.prev_pos = self.next_pos

    def handle_generate_data_event(self, curr_time):
        chunk = self._data_generator.generate_data_chunk(curr_time)
        return chunk, self.strategy.handle_generate_data_event(chunk, curr_time)

    def handle_connection_event(self, node, curr_time):
        return self.strategy.handle_connection_event(node, curr_time)

    def handle_disconnection_event(self, node, curr_time):
        return self.strategy.handle_disconnection_event(node, curr_time)

    def handle_transfer_data_chunk(self, node, curr_time, chunk, chunk_owner):
        return self.strategy.handle_transfer_data_chunk(node, curr_time, chunk, chunk_owner)

    def handle_receive_data_chunk(self, node, curr_time, chunk, chunk_owner):
        return self.strategy.handle_receive_data_chunk(node, curr_time, chunk, chunk_owner)

    def get_next_data_gen_time(self):
        return self._data_generator.get_next_time()

    @property
    def end_move_time(self):
        return self.start_move_time + self.move_time

    @property
    def move_time(self):
        if self.next_pos is None or self.prev_pos is None:
            return None
        if self.speed == 0:
            return None
        d = mathutils.distance(self.prev_pos, self.next_pos)
        return d / self.speed

    @property
    def velocity(self):
        return mathutils.unit_vector_between_points(self.prev_pos, self.next_pos) * self.speed

    def __repr__(self):
        return "".join(["MovingNode: lr: ", str(self.transfer_module.link_radius),
                        ", v: ", str(self.speed), ", mr: ", str(self.move_radius)])
