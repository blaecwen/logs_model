from collections import defaultdict

from logs_model.events import ChunkTransferEvent
from logs_model.nodes import Node


class TransferModule(object):
    def __init__(self, speed, link_radius):
        self.speed = speed
        self.link_radius = link_radius
        self.connections = defaultdict(lambda: [False, 0, False])   # (is_connected, connection_id, is_used)
        self._owner = None

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    def _add_connection(self, tm):
        assert not self.connections[tm][0]
        self.connections[tm][0] = True
        self.connections[tm][1] += 1
        self.connections[tm][2] = False

    def _del_connection(self, tm):
        assert self.connections[tm][0]
        self.connections[tm][0] = False

    def is_connection_up(self, tm, connection_id):
        if not self.connections[tm][0]:                    # means was disconnect during transfer
            return False
        if self.connections[tm][1] != connection_id:       # means was disconnect and connect during transfer
            return False
        return True

    def get_server_connection(self):
        """
        Returns arbitrary server connection if connected to a server

        Return: None or TransferModule
        """
        for tm, conn in self.connections.items():
            if type(tm.owner) == Node and conn[0]:
                return tm
        return None

    def handle_connection_event(self, tm, curr_time):
        self._add_connection(tm)
        return self.owner.handle_connection_event(tm, curr_time)

    def handle_disconnection_event(self, tm, curr_time):
        self._del_connection(tm)
        return self.owner.handle_disconnection_event(tm, curr_time)

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        self.connections[tm][2] = False
        return self.owner.handle_transfer_data_chunk(tm, curr_time, chunk, chunk_owner)

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        return self.owner.handle_receive_data_chunk(tm, curr_time, chunk, chunk_owner)

    def start_data_chunk_transfer(self, tm_dst, chunk, curr_time, chunk_owner):
        assert self.connections[tm_dst][0]        # connection should exist
        assert not self.connections[tm_dst][2]    # connection should not be used
        event = ChunkTransferEvent(self, tm_dst, self.connections[tm_dst][1],
                                   chunk, curr_time, chunk.amount / self.speed, chunk_owner)
        self.connections[tm_dst][2] = True
        return [event]
