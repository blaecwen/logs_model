from random import uniform

from logs_model.events import *

from logs_model.nodes import *


class Field(object):
    def __init__(self, corner, width, height):
        self.corner = corner
        self.width = width
        self.height = height

        self.nodes = {}
        self.broken_nodes = {}
        self.break_node_times = {}
        self.last_node_id = 0

    def add_node(self, node, pos, curr_time):
        if not self.contains(pos):
            return None
        node.prev_pos = pos

        node.id = self.last_node_id
        self.last_node_id += 1
        self.nodes[node.id] = node

        events = []
        if isinstance(node, MovingNode):
            events += [GenerateDataEvent(curr_time, node)]
            events += [MoveEvent(self, curr_time, node)]

        events += gen_startup_transfer_events(node, self.nodes, curr_time)
        events += gen_new_transfer_events_for_node(node, self.nodes, curr_time)
        return events

    def break_node(self, node_id, curr_time):
        node = self.nodes[node_id]
        del self.nodes[node_id]
        self.broken_nodes[node_id] = node
        self.break_node_times[node_id] = curr_time

    def contains(self, point):
        return (self.corner.x <= point.x <= self.corner.x + self.width and
                self.corner.y <= point.y <= self.corner.y + self.height)

    def pick_random_point(self):
        return mathutils.Point(uniform(self.corner.x, self.corner.x + self.width),
                               uniform(self.corner.y, self.corner.y + self.height))

    def __repr__(self):
        return "".join(["Corner: ", str(self.corner), ", w: ", str(self.width), ", h: ", str(self.height)])
