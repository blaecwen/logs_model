
tformat = "%.2f"
# tformat = "%d"

log_file = None


def log_message(message: str="", time: float=None, color: str=None):
    tcolors = {
        "purple": '\033[95m',
        "blue": '\033[94m',
        "green": '\033[92m',
        "yellow": '\033[93m',
        "red": '\033[91m',
        "bold": '\033[1m',
        "underline": '\033[4m',
        "none": ''
    }
    color_end = '\033[0m'
    if color is None:
        color = "none"
        color_end = ""

    time_str = ""
    if time is not None:
        time_str = tformat % time + ": "

    print(tcolors[color] + time_str + message + color_end, file=log_file)


def iter_priority_queue(queue):

    if queue.empty():
        return

    q = queue.queue
    next_indices = [0]
    while next_indices:
        min_index = min(next_indices, key=q.__getitem__)
        yield q[min_index]
        next_indices.remove(min_index)
        if 2 * min_index + 1 < len(q):
            next_indices.append(2 * min_index + 1)
        if 2 * min_index + 2 < len(q):
            next_indices.append(2 * min_index + 2)


def dfs(root, exclude=None):
    """
    Depth-first search for nodes network

    :param root: TransferModule of node to start from
    :param exclude: list of TransferModules of nodes to ignore
    :return: generator to iterate over TransferModules
    """
    if exclude is None:
        discovered = []
    else:
        discovered = exclude

    stack = [root]
    while stack:
        v_tm = stack.pop()
        if v_tm in discovered:
            continue

        yield v_tm

        discovered.append(v_tm)
        for t, conn in v_tm.connections.items():
            if not conn[0]:
                continue
            stack.append(t)
