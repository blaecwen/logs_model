
import random
import math
import cmath
import numpy as np


class Point:
    def __init__(self, x_init, y_init):
        self.x = x_init
        self.y = y_init

    def shift(self, x, y):
        self.x += x
        self.y += y

    def to_np_array(self):
        return np.array([self.x, self.y])

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return "".join(["Point(", str(self.x), ",", str(self.y), ")"])


class Disk:
    def __init__(self, center, radius):
        assert radius > 0
        self.center = center
        self.radius = radius

    def __repr__(self):
        return "".join(["Center(", str(self.center[0]), ",", str(self.center[1]), ")", ";R=", str(self.radius)])


def distance(a, b):
    """
    distance between point a and b

    :param a: instance of Point
    :param b: instance of Point
    :return: floating point
    """
    return math.sqrt((a.x-b.x)**2+(a.y-b.y)**2)


# TODO: It is not uniform distribution!!
def random_point_in_circle(center, radius):
    r = random.random()
    a = random.uniform(0, 2*math.pi)
    x = r*radius * math.cos(a)
    y = r*radius * math.sin(a)
    return Point(x + center.x, y + center.y)


def solve_quadratic_equation(a, b, c):
    if a == 0:
        return None
    # calculate the discriminant
    d = (b**2) - (4*a*c)

    # find two solutions
    sqrt_d = cmath.sqrt(d)
    sol1 = (-b - sqrt_d) / (2*a)
    sol2 = (-b + sqrt_d) / (2*a)
    return sol1, sol2


def unit_vector_between_points(from_point, to_point):
    if from_point == to_point:
        return np.array([0, 0])
    r1 = from_point.to_np_array()
    r2 = to_point.to_np_array()
    r = r2 - r1
    return r / np.linalg.norm(r)


def is_overlapping(d1, d2):
    """
    Checks if |d1.center - d2.center| <= min(R1, R2)
    :param d1: disk 1
    :param d2: disk 2
    :return: True if overlaps
    """

    d = d1.center - d2.center
    return np.linalg.norm(d) <= min(d1.radius, d2.radius)


def calc_disks_overlapping_times(d1, v1, d2, v2):
    """
    Calculates times of disks overlapping using kinematics and linear algebra
    Under overlapping is considered this condition:
    |d1.center - d2.center| <= min(R1, R2)

    :param d1: disk 1
    :param v1: velocity of disk 1
    :param d2: disk 2
    :param v2: velocity of disk 2
    :return: (t1, t2) times of disks overlapping
    """

    # calculated almost like calc_disks_collide_times

    d = min(d1.radius, d2.radius)**2
    a1 = d1.center[0] - d2.center[0]
    a2 = d1.center[1] - d2.center[1]
    b1 = v1[0] - v2[0]
    b2 = v1[1] - v2[1]

    if b1 == 0 and b2 == 0:
        return None

    a = b1**2 + b2**2
    b = 2*(a1*b1 + a2*b2)
    c = a1**2 + a2**2 - d
    res = solve_quadratic_equation(a, b, c)
    if not np.isreal(res[0]):
        return None
    return np.real(res[0]), np.real(res[1])


def calc_disks_collide_times(d1, v1, d2, v2):
    """
    Calculates times of disks touching using kinematics and linear algebra.

    :param d1: disk 1
    :param v1: velocity of disk 1
    :param d2: disk 2
    :param v2: velocity of disk 2
    :return: (t1, t2) times of disks touching
    """
    # (R1+R2)**2 = (x1(t) - x2(t))**2 +
    #              (y1(t) - y2(t))**2
    # where x1(t) = x10 + vx1*t
    # x2, y1, y2 - same as x1

    #     d         (   a1  )   (  b1   )
    # (R1+R2)**2 = ((x10-x20) + (vx1-vx2)*t)**2 +
    #              ((y10-y20) + (vy1-vy2)*t)**2
    #               (   a2  )   (  b2   )

    # d = (a1 + b1*t)**2 + (a2 + b2*t)**2

    d = (d1.radius + d2.radius)**2
    a1 = d1.center[0] - d2.center[0]
    a2 = d1.center[1] - d2.center[1]
    b1 = v1[0] - v2[0]
    b2 = v1[1] - v2[1]

    if b1 == 0 and b2 == 0:
        return None

    a = b1**2 + b2**2
    b = 2*(a1*b1 + a2*b2)
    c = a1**2 + a2**2 - d
    res = solve_quadratic_equation(a, b, c)
    if not np.isreal(res[0]):
        return None
    return np.real(res[0]), np.real(res[1])


class LinearFunction(object):
    def __init__(self, speed):
        self.speed = speed

    def __call__(self, t1, t2):
        return (t2 - t1)*self.speed
