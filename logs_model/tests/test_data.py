#!/usr/bin/python3

import unittest

from logs_model.data import *


class TestDataGenerator(unittest.TestCase):
    def test_init(self):
        dg = DataGenerator(10, 2, 100, 10)  # period, period_delta, amount, amount_delta

        self.assertEqual(dg.period, 10)
        self.assertEqual(dg.period_delta, 2)
        self.assertEqual(dg.amount, 100)
        self.assertEqual(dg.amount_delta, 10)

    def test_generate_data_chunk(self):
        amount = 100
        amount_delta = 10
        dg = DataGenerator(10, 2, amount, amount_delta)

        for i in range(100):
            e = dg.generate_data_chunk(10)
            self.assertEqual(e.time, 10)
            self.assertEqual(e.id, i)
            self.assertLessEqual(e.amount, amount + amount_delta)
            self.assertGreaterEqual(e.amount, amount - amount_delta)

    def test_get_next_time(self):
        period = 100
        period_delta = 10
        dg = DataGenerator(period, period_delta, 10, 2)

        for i in range(100):
            t = dg.get_next_time()
            self.assertLessEqual(t, period + period_delta)
            self.assertGreaterEqual(t, period - period_delta)


class TestMetaData(unittest.TestCase):
    def test_init(self):
        md = MetaData()
        self.assertEqual(md.horizon_data_id, 0)
        self.assertEqual(len(md.data), 0)

        md = MetaData(10)
        self.assertEqual(md.horizon_data_id, 10)
        self.assertEqual(len(md.data), 0)

    def test_contains(self):
        ld = MetaData(100, [102, 105, 110])
        e0 = LogDataEntry(1, 1, 10)
        e1 = LogDataEntry(1, 1, 100)
        e2 = LogDataEntry(1, 1, 102)
        e3 = LogDataEntry(2, 2, 103)
        e4 = LogDataEntry(2, 3, 200)
        e5 = LogDataEntry(2, 3, 110)

        self.assertEqual(True, e0 in ld)
        self.assertEqual(False, e1 in ld)
        self.assertEqual(True, e2 in ld)
        self.assertEqual(False, e3 in ld)
        self.assertEqual(False, e4 in ld)
        self.assertEqual(True, e5 in ld)

    def test_add_chunk(self):
        md = MetaData()
        e1 = LogDataEntry(10, 10, 0)

        e2 = LogDataEntry(10, 10, 10)
        e3 = LogDataEntry(10, 10, 1)

        e4 = LogDataEntry(10, 10, 5)
        e5 = LogDataEntry(10, 10, 3)
        e6 = LogDataEntry(10, 10, 4)
        e7 = LogDataEntry(10, 10, 7)
        e8 = LogDataEntry(10, 10, 8)
        e9 = LogDataEntry(10, 10, 2)

        e10 = LogDataEntry(10, 10, 6)

        e11 = LogDataEntry(10, 10, 9)

        md.add_chunk(e1)
        self.assertEqual(md.horizon_data_id, 1)
        self.assertEqual(len(md.data), 0)

        md.add_chunk(e2)
        self.assertEqual(md.horizon_data_id, 1)
        self.assertEqual(len(md.data), 1)

        md.add_chunk(e3)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 1)

        md.add_chunk(e4)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 2)

        md.add_chunk(e5)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 3)

        md.add_chunk(e6)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 4)

        md.add_chunk(e7)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 5)

        md.add_chunk(e8)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 6)

        md.add_chunk(e8)
        self.assertEqual(md.horizon_data_id, 2)
        self.assertEqual(len(md.data), 6)

        md.add_chunk(e9)
        self.assertEqual(md.horizon_data_id, 6)
        self.assertEqual(len(md.data), 3)

        md.add_chunk(e10)
        self.assertEqual(md.horizon_data_id, 9)
        self.assertEqual(len(md.data), 1)

        md.add_chunk(e11)
        self.assertEqual(md.horizon_data_id, 11)
        self.assertEqual(len(md.data), 0)

    def test_sync_meta_two_empty(self):
        m1 = MetaData()
        m2 = MetaData()
        m1.sync_meta(m2)
        self._assertMetaData(m1, 0, [])
        self._assertMetaData(m1, 0, [])

        m2.sync_meta(m1)
        self._assertMetaData(m1, 0, [])
        self._assertMetaData(m1, 0, [])

    def test_sync_meta_one_empty(self):
        h1 = 10
        l1 = [11, 13, 15]
        m1 = MetaData(h1, l1)
        m2 = MetaData()
        m3 = MetaData()

        m1.sync_meta(m2)
        self._assertMetaData(m1, h1, l1)
        self._assertMetaData(m2, h1, l1)

        m3.sync_meta(m2)
        self._assertMetaData(m1, h1, l1)
        self._assertMetaData(m2, h1, l1)
        self._assertMetaData(m3, h1, l1)

        m1.sync_meta(m3)
        self._assertMetaData(m1, h1, l1)
        self._assertMetaData(m2, h1, l1)
        self._assertMetaData(m3, h1, l1)

    def test_sync_meta_horizon(self):
        h1, h2 = 10, 4
        l1 = [11, 13, 15]
        m1 = MetaData(h1, l1)
        m2 = MetaData(h2, l1)
        m3 = MetaData(h2, l1)

        m1.sync_meta(m2)
        self._assertMetaData(m1, h1, l1)
        self._assertMetaData(m2, h1, l1)

        m3.sync_meta(m2)
        self._assertMetaData(m1, h1, l1)
        self._assertMetaData(m2, h1, l1)
        self._assertMetaData(m3, h1, l1)

    def test_sync_meta_data(self):
        h = 10
        l1 = [11, 13, 15]
        l2 = [100, 13]
        l3 = [20, 15]

        m1 = MetaData(h, l1)
        m2 = MetaData(h, l2)
        m3 = MetaData(h, l3)

        lm1 = [11, 13, 15, 100]
        m1.sync_meta(m2)
        self._assertMetaData(m1, h, lm1)
        self._assertMetaData(m2, h, lm1)

        lm2 = [11, 13, 15, 20, 100]
        m3.sync_meta(m2)
        self._assertMetaData(m1, h, lm1)
        self._assertMetaData(m2, h, lm2)
        self._assertMetaData(m3, h, lm2)

    def test_sync_meta(self):
        h1, h2, h3, h4 = 10, 15, 5, 1000
        l1 = [11, 13, 15, 20, 25]
        l2 = [16, 17, 20, 27]
        l3 = [7, 8, 30]
        l4 = [1001, 1002]

        m1 = MetaData(h1, l1)
        m2 = MetaData(h2, l2)
        m3 = MetaData(h3, l3)
        m4 = MetaData(h4, l4)

        hr1 = 18
        lr1 = [20, 25, 27]
        m1.sync_meta(m2)
        self._assertMetaData(m1, hr1, lr1)
        self._assertMetaData(m2, hr1, lr1)

        hr2 = 18
        lr2 = [20, 25, 27, 30]
        m3.sync_meta(m2)
        self._assertMetaData(m1, hr1, lr1)
        self._assertMetaData(m2, hr2, lr2)
        self._assertMetaData(m3, hr2, lr2)

        hr3 = 1000
        lr3 = [1001, 1002]
        m4.sync_meta(m3)
        self._assertMetaData(m1, hr1, lr1)
        self._assertMetaData(m2, hr2, lr2)
        self._assertMetaData(m3, hr3, lr3)
        self._assertMetaData(m3, hr3, lr3)

    def test_sync_meta_beautiful(self):
        h1, h2 = 7, 8
        l1 = [x + 8 for x in range(100) if x % 2 == 0]
        l2 = [x + 9 for x in range(100) if x % 2 == 0]

        m1 = MetaData(h1, l1)
        m2 = MetaData(h2, l2)
        hr = 108
        lr = []
        m1.sync_meta(m2)
        self._assertMetaData(m1, hr, lr)
        self._assertMetaData(m2, hr, lr)

    def _assertMetaData(self, meta, hr, ls):
        self.assertEqual(meta.horizon_data_id, hr)
        self.assertEqual(list(meta.data), ls)


class TestLogData(unittest.TestCase):
    def test_log_data_entry(self):
        e1 = LogDataEntry(1, 2, 3)
        e2 = LogDataEntry(1, 2, 3)
        e3 = LogDataEntry(2, 2, 3)
        self.assertEqual(e1.amount, 1)
        self.assertEqual(e1.time, 2)
        self.assertEqual(e1.id, 3)

        self.assertEqual(e1, e2)
        self.assertNotEqual(e1, e3)
        self.assertNotEqual(e2, e3)

    def test_init(self):
        ld = LogData()

        self.assertEqual(ld.total, 0)
        self.assertEqual(ld.time_start, float("inf"))
        self.assertEqual(ld.time_end, 0)
        self.assertEqual(len(ld._data), 0)

    def test_contains(self):
        ld = LogData()
        e0 = LogDataEntry(1, 1, 1)
        e1 = LogDataEntry(1, 1, 1)
        e2 = LogDataEntry(2, 2, 2)
        e3 = LogDataEntry(2, 3, 3)

        def test_ld(el):
            self.assertEqual(el[0], e0 in ld)
            self.assertEqual(el[1], e1 in ld)
            self.assertEqual(el[2], e2 in ld)
            self.assertEqual(el[3], e3 in ld)

        test_ld([False, False, False, False])
        ld.append_data(e1)
        test_ld([True, True, False, False])
        ld.append_data(e2)
        test_ld([True, True, True, False])
        ld.append_data(e3)
        test_ld([True, True, True, True])

    def test_clear(self):
        ld = LogData()
        ld.append_data(LogDataEntry(10, 20, 0))
        ld.clear()

        self.assertEqual(ld.total, 0)
        self.assertEqual(ld.time_start, float("inf"))
        self.assertEqual(ld.time_end, 0)
        self.assertEqual(len(ld._data), 0)

        ld.append_data(LogDataEntry(10, 20, 0))
        self.assertEqual(ld.total, 10)
        self.assertEqual(ld.time_start, 20)
        self.assertEqual(ld.time_end, 20)
        self.assertEqual(len(ld._data), 1)

    def test_append_data(self):
        ld = LogData()
        e1 = LogDataEntry(10, 20, 0)    # amount, time, entry_id
        e2 = LogDataEntry(50, 70, 10)
        e3 = LogDataEntry(30, 35, 100)

        ld.append_data(e1)
        self._assertLogData(ld, 20, 20, 10, 1)

        ld.append_data(e2)
        self._assertLogData(ld, 20, 70, 60, 2)

        ld.append_data(e3)
        self._assertLogData(ld, 20, 70, 90, 3)

        ld.append_data(LogDataEntry(0, 100, 12))
        self._assertLogData(ld, 20, 70, 90, 3)

    def test_update_committed(self):
        ld = LogData()
        c = [
            LogDataEntry(10, 10, 0),
            LogDataEntry(10, 11, 1),
            LogDataEntry(10, 12, 2),
            LogDataEntry(10, 13, 3),
            LogDataEntry(10, 14, 4),
            LogDataEntry(10, 15, 5),
            LogDataEntry(10, 16, 10)
        ]
        for e in c:
            ld.append_data(e)

        md = MetaData()
        ld.update_committed(md)     # 0, 1, 2, 3, 4, 5, 10
        self._assertLogData(ld, 10, 16, 70, 7)

        md.add_chunk(LogDataEntry(10, 10, 100))
        ld.update_committed(md)     # 0, 1, 2, 3, 4, 5, 10
        self._assertLogData(ld, 10, 16, 70, 7)

        md.add_chunk(c[0])
        md.add_chunk(c[1])
        md.add_chunk(c[4])
        md.add_chunk(c[6])
        ld.update_committed(md)     # 2, 3, 5
        self._assertLogData(ld, 12, 15, 30, 3)

        md.add_chunk(c[5])
        ld.update_committed(md)     # 2, 3
        self._assertLogData(ld, 12, 13, 20, 2)

        md.add_chunk(c[3])
        md.add_chunk(c[2])
        ld.update_committed(md)     # Empty
        self._assertLogData(ld, float("inf"), 0, 0, 0)

    def test_get_earliest_chunk(self):
        ld = LogData()
        self.assertIsNone(ld.get_earliest_chunk())

        e1 = LogDataEntry(10, 10, 0)
        e2 = LogDataEntry(20, 16, 10)
        e3 = LogDataEntry(30, 20, 12)

        ld.append_data(e2)
        self.assertEqual(ld.get_earliest_chunk(), e2)

        ld.append_data(e1)
        self.assertEqual(ld.get_earliest_chunk(), e1)

        ld.append_data(e3)
        self.assertEqual(ld.get_earliest_chunk(), e1)

    def test_get_earliest_chunk_from_complement(self):
        ld = LogData()
        l1 = LogData()
        l2 = LogData()
        l2.append_data(LogDataEntry(10, 10, 102))
        l2.append_data(LogDataEntry(10, 10, 105))
        l2.append_data(LogDataEntry(10, 10, 200))
        e1 = LogDataEntry(10, 10, 200)          # amount, time, id
        e3 = LogDataEntry(10, 10, 106)
        e4 = LogDataEntry(10, 10, 104)
        e5 = LogDataEntry(10, 10, 102)

        c1 = ld.get_earliest_chunk_from_complement(l1)
        c2 = ld.get_earliest_chunk_from_complement(l2)
        self.assertIsNone(c1)
        self.assertIsNone(c2)

        ld.append_data(e1)
        c1 = ld.get_earliest_chunk_from_complement(l1)
        c2 = ld.get_earliest_chunk_from_complement(l2)
        self.assertEqual(e1, c1)
        self.assertIsNone(c2)

        ld.append_data(e3)
        c1 = ld.get_earliest_chunk_from_complement(l1)
        c2 = ld.get_earliest_chunk_from_complement(l2)
        self.assertEqual(e3, c1)
        self.assertEqual(e3, c2)

        ld.append_data(e4)
        c1 = ld.get_earliest_chunk_from_complement(l1)
        c2 = ld.get_earliest_chunk_from_complement(l2)
        self.assertEqual(e4, c1)
        self.assertEqual(e4, c2)

        ld.append_data(e5)
        c1 = ld.get_earliest_chunk_from_complement(l1)
        c2 = ld.get_earliest_chunk_from_complement(l2)
        self.assertEqual(e5, c1)
        self.assertEqual(e4, c2)

    def test_get_earliest_chunk_meta(self):
        ld = LogData()
        m1 = MetaData()
        m2 = MetaData(100, [102, 105, 200])
        e1 = LogDataEntry(10, 10, 200)          # amount, time, id
        e2 = LogDataEntry(10, 10, 10)
        e3 = LogDataEntry(10, 10, 104)
        e4 = LogDataEntry(10, 10, 100)

        c1 = ld.get_earliest_chunk_meta(m1)
        c2 = ld.get_earliest_chunk_meta(m2)
        self.assertIsNone(c1)
        self.assertIsNone(c2)

        ld.append_data(e1)
        c1 = ld.get_earliest_chunk_meta(m1)
        c2 = ld.get_earliest_chunk_meta(m2)
        self.assertEqual(e1, c1)
        self.assertIsNone(c2)

        ld.append_data(e2)
        c1 = ld.get_earliest_chunk_meta(m1)
        c2 = ld.get_earliest_chunk_meta(m2)
        self.assertEqual(e2, c1)
        self.assertIsNone(c2)

        ld.append_data(e3)
        c1 = ld.get_earliest_chunk_meta(m1)
        c2 = ld.get_earliest_chunk_meta(m2)
        self.assertEqual(e2, c1)
        self.assertEqual(e3, c2)

        ld.append_data(e4)
        c1 = ld.get_earliest_chunk_meta(m1)
        c2 = ld.get_earliest_chunk_meta(m2)
        self.assertEqual(e2, c1)
        self.assertEqual(e4, c2)

    def test_delete_chunk(self):
        ld = LogData()
        e1 = LogDataEntry(10, 10, 0)
        e2 = LogDataEntry(20, 16, 10)
        e3 = LogDataEntry(30, 20, 12)
        e4 = LogDataEntry(42, 42, 42)
        ld.append_data(e1)
        ld.append_data(e2)
        ld.append_data(e3)
        ld.append_data(e4)

        ld.delete_chunk(e3)
        self._assertLogData(ld, 10, 42, 72, 3)

        self.assertRaises(KeyError, ld.delete_chunk, e3)
        self._assertLogData(ld, 10, 42, 72, 3)

        ld.delete_chunk(e4)
        self._assertLogData(ld, 10, 16, 30, 2)

        ld.delete_chunk(e1)
        self._assertLogData(ld, 16, 16, 20, 1)

        self.assertRaises(AssertionError, ld.delete_chunk, e1)
        self._assertLogData(ld, 16, 16, 20, 1)

        ld.delete_chunk(e2)
        self._assertLogData(ld, float("inf"), 0, 0, 0)

    def _assertLogData(self, ld, t0, t1, total, length):
                self.assertEqual(ld.time_start, t0)
                self.assertEqual(ld.time_end, t1)
                self.assertEqual(ld.total, total)
                self.assertEqual(len(ld._data), length)


if __name__ == '__main__':
    unittest.main()
