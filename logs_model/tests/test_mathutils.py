#!/usr/bin/python3

import unittest

from logs_model.mathutils import *

class TestVectorMath(unittest.TestCase):
    def test_unit_vector_between_points(self):
        v = unit_vector_between_points(Point(1, 0), Point(1, 0))
        self.assertTrue(np.array_equal(np.array([0, 0]), v))

        v = unit_vector_between_points(Point(0, 0), Point(1, 0))
        self.assertTrue(np.array_equal(np.array([1, 0]), v))

        v = unit_vector_between_points(Point(2, -3), Point(2, 3))
        self.assertTrue(np.array_equal(np.array([0, 1]), v))

        v = unit_vector_between_points(Point(1, 0), Point(2, 0))
        self.assertTrue(np.array_equal(np.array([1, 0]), v))

    def test_is_overlapping(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([10, 0]), 10)
        self.assertTrue(is_overlapping(d1, d2))

        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([0, 0]), 10)
        self.assertTrue(is_overlapping(d1, d2))

        d1 = Disk(np.array([0, 0]), 100)
        d2 = Disk(np.array([10, 10]), 10)
        self.assertFalse(is_overlapping(d1, d2))

        d1 = Disk(np.array([0, 0]), 100)
        d2 = Disk(np.array([6, 8]), 10)
        self.assertTrue(is_overlapping(d1, d2))

        d1 = Disk(np.array([0, 0]), 100)
        d2 = Disk(np.array([6, 8]), 9.999)
        self.assertFalse(is_overlapping(d1, d2))


class TestCalcDisksCollideTimes(unittest.TestCase):
    def test_collision_x(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([100, 0]), 10)

        v1 = np.array([10, 0])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (8, 12))

        v1 = np.array([10, 0])
        v2 = np.array([-10, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (4, 6))

        v1 = np.array([20, 0])
        v2 = np.array([10, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (8, 12))

    def test_collision_y(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([0, 100]), 10)

        v1 = np.array([0, 10])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (8, 12))

        v1 = np.array([0, 10])
        v2 = np.array([0, -10])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (4, 6))

        v1 = np.array([0, 20])
        v2 = np.array([0, 10])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (8, 12))

    def test_collision_xy(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([30, 40]), 10)

        v1 = np.array([3, 4])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (6, 14))

        v1 = np.array([3, 4])
        v2 = np.array([-3, -4])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (3, 7))

        v1 = np.array([6, 8])
        v2 = np.array([3, 4])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (6, 14))

        d1 = Disk(np.array([0, 0]), 2.5)
        d2 = Disk(np.array([13, 14]), 2.5)
        v1 = np.array([1, 0])
        v2 = np.array([0, -1])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (10, 17))

    def test_negative_times(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([100, 0]), 10)
        v1 = np.array([-10, 0])
        v2 = np.array([10, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (-6, -4))

        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([30, 40]), 10)

        v1 = np.array([-3, -4])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (-14, -6))

        v1 = np.array([-3, -4])
        v2 = np.array([3, 4])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (-7, -3))

    def test_touch(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([0, 20]), 10)

        v1 = np.array([10, 0])
        v2 = np.array([20, 0])
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (0, 0))

        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([-10, 20]), 10)
        self.assertEqual(calc_disks_collide_times(d1, v1, d2, v2), (1, 1))

    def test_same_velocity(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([0, 20]), 10)

        v1 = np.array([0, 0])
        v2 = np.array([0, 0])
        self.assertIsNone(calc_disks_collide_times(d1, v1, d2, v2))

        v1 = np.array([10, 10])
        v2 = np.array([10, 10])
        self.assertIsNone(calc_disks_collide_times(d1, v1, d2, v2))

    def test_no_collision(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([100, 0]), 10)

        v1 = np.array([10, 10])
        v2 = np.array([-10, -10])
        self.assertIsNone(calc_disks_collide_times(d1, v1, d2, v2))

        v1 = np.array([0, 10])
        v2 = np.array([10, 0])
        self.assertIsNone(calc_disks_collide_times(d1, v1, d2, v2))


class TestCalcDisksOverlappingTimes(unittest.TestCase):
    def test_collision_x(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([100, 0]), 10)

        v1 = np.array([10, 0])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (9, 11))

        v1 = np.array([10, 0])
        v2 = np.array([-10, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (4.5, 5.5))

        v1 = np.array([20, 0])
        v2 = np.array([10, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (9, 11))

    def test_collision_y(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([0, 100]), 10)

        v1 = np.array([0, 10])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (9, 11))

        v1 = np.array([0, 10])
        v2 = np.array([0, -10])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (4.5, 5.5))

        v1 = np.array([0, 20])
        v2 = np.array([0, 10])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (9, 11))

    def test_collision_xy(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([30, 40]), 10)

        v1 = np.array([3, 4])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (8, 12))

        v1 = np.array([3, 4])
        v2 = np.array([-3, -4])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (4, 6))

        v1 = np.array([6, 8])
        v2 = np.array([3, 4])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (8, 12))

        d1 = Disk(np.array([0, 0]), 5)
        d2 = Disk(np.array([13, 14]), 5)
        v1 = np.array([1, 0])
        v2 = np.array([0, -1])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (10, 17))

    def test_negative_times(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([100, 0]), 10)
        v1 = np.array([-10, 0])
        v2 = np.array([10, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (-5.5, -4.5))

        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([30, 40]), 10)

        v1 = np.array([-3, -4])
        v2 = np.array([0, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (-12, -8))

        v1 = np.array([-3, -4])
        v2 = np.array([3, 4])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (-6, -4))

    def test_touch(self):
        d1 = Disk(np.array([0, 0]), 20)
        d2 = Disk(np.array([0, 20]), 100)

        v1 = np.array([10, 0])
        v2 = np.array([20, 0])
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (0, 0))

        d1 = Disk(np.array([0, 0]), 100)
        d2 = Disk(np.array([-10, 20]), 20)
        self.assertEqual(calc_disks_overlapping_times(d1, v1, d2, v2), (1, 1))

    def test_same_velocity(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([0, 20]), 10)

        v1 = np.array([0, 0])
        v2 = np.array([0, 0])
        self.assertIsNone(calc_disks_overlapping_times(d1, v1, d2, v2))

        v1 = np.array([10, 10])
        v2 = np.array([10, 10])
        self.assertIsNone(calc_disks_overlapping_times(d1, v1, d2, v2))

    def test_no_collision(self):
        d1 = Disk(np.array([0, 0]), 10)
        d2 = Disk(np.array([100, 0]), 10)

        v1 = np.array([10, 10])
        v2 = np.array([-10, -10])
        self.assertIsNone(calc_disks_overlapping_times(d1, v1, d2, v2))

        v1 = np.array([0, 10])
        v2 = np.array([10, 0])
        self.assertIsNone(calc_disks_overlapping_times(d1, v1, d2, v2))


if __name__ == '__main__':
    unittest.main()
