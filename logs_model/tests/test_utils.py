#!/usr/bin/python3

import unittest
from queue import PriorityQueue

from logs_model.utils import *
from logs_model.transfer_modules import TransferModule


class TestIterPriorityQueue(unittest.TestCase):
    def test_iter(self):
        l = [1, 5, 10]
        q = PriorityQueue()
        for elem in l:
            q.put(elem)

        i = 0
        for elem in iter_priority_queue(q):
            self.assertEqual(elem, l[i])
            i += 1
        self.assertEqual(i, 3)

        l = [11, 10, 5, 1]
        q = PriorityQueue()
        for elem in l:
            q.put(elem)

        i = 0
        for elem in iter_priority_queue(q):
            self.assertEqual(elem, l[3-i])
            i += 1
        self.assertEqual(i, 4)

    def test_empty(self):
        q = PriorityQueue()
        correct = True
        for _ in iter_priority_queue(q):
            correct = False

        self.assertTrue(correct)


class TestDFS(unittest.TestCase):
    def test_dfs(self):
        num = 8
        t = []
        for i in range(num):
            t.append(TransferModule(1, 1))

        for i in range(num-1):
            t[7].connections[t[i]][0] = False
            t[i].connections[t[7]][0] = False

        g = dfs(t[0])
        self._assertGenerator({t[0]}, g)

        self._set_link(t, 0, 1)
        g = dfs(t[0])
        self._assertGenerator({t[0], t[1]}, g)

        g = dfs(t[0], [t[1]])
        self._assertGenerator({t[0]}, g)

        self._set_link(t, 0, 2)
        g = dfs(t[0])
        self._assertGenerator({t[0], t[1], t[2]}, g)

        self._set_link(t, 3, 4)
        g = dfs(t[0])
        self._assertGenerator({t[0], t[1], t[2]}, g)
        g = dfs(t[3])
        self._assertGenerator({t[4], t[3]}, g)

        self._set_link(t, 1, 3)
        g = dfs(t[0])
        self._assertGenerator({t[0], t[1], t[2], t[3], t[4]}, g)
        g = dfs(t[3])
        self._assertGenerator({t[0], t[1], t[2], t[3], t[4]}, g)

        self._set_link(t, 2, 4)
        for i in range(5):
            g = dfs(t[i])
            self._assertGenerator({t[0], t[1], t[2], t[3], t[4]}, g)

        self._set_link(t, 0, 5)
        for i in range(6):
            g = dfs(t[i])
            self._assertGenerator({t[0], t[1], t[2], t[3], t[4], t[5]}, g)

        self._set_link(t, 5, 6)
        for i in range(7):
            g = dfs(t[i])
            self._assertGenerator({t[0], t[1], t[2], t[3], t[4], t[5], t[6]}, g)

        for i in range(5):
            g = dfs(t[i], [t[5]])
            self._assertGenerator({t[0], t[1], t[2], t[3], t[4]}, g)

        for i in [0, 1, 3, 4, 5, 6]:
            g = dfs(t[i], [t[2]])
            self._assertGenerator({t[0], t[1], t[3], t[4], t[5], t[6]}, g)

    def _assertGenerator(self, traverse_set, gen):
        discovered = set()
        for tm in gen:
            self.assertNotIn(tm, discovered)
            self.assertIn(tm, traverse_set)
            discovered.add(tm)
        self.assertEqual(traverse_set, discovered)

    @staticmethod
    def _set_link(t, n1, n2):
        t[n1].connections[t[n2]][0] = True
        t[n2].connections[t[n1]][0] = True

if __name__ == '__main__':
    unittest.main()
