#!/usr/bin/python3

import unittest


# class TestEvent(unittest.TestCase):
#     def test_init(self):
#         self.assertEqual(Event(0, 10, 0, None).start_time, 0)
#         self.assertEqual(Event(0, 10, 0, None).end_time, 10)
#         self.assertEqual(Event(20, 20, 0, None).end_time, 20)
#         self.assertEqual(Event(20, 20, 0, None).start_time, 20)
#
#         with self.assertRaises(AssertionError):
#             Event(-1, 1, 0, None)
#         with self.assertRaises(AssertionError):
#             Event(1, -1, 0, None)
#         with self.assertRaises(AssertionError):
#             Event(10, 4, 0, None)
#
#     def test_priority(self):
#         self.assertTrue(Event(0, 10, 0, None) < Event(0, 20, 0, None))
#         self.assertFalse(Event(0, 20, 0, None) < Event(0, 20, 0, None))
#         self.assertFalse(Event(0, 20, 0, None) < Event(0, 10.0, 0, None))
#
#         self.assertFalse(Event(0, 10, 0, None) > Event(0, 20, 0, None))
#         self.assertFalse(Event(0, 20, 0, None) > Event(0, 20, 0, None))
#         self.assertTrue(Event(0, 20, 0, None) > Event(0, 10.0, 0, None))
#
#         self.assertTrue(Event(0, 20, 0, None) == Event(0, 20, 0, None))
#         self.assertFalse(Event(0, 10, 0, None) == Event(0, 20, 0, None))
#
#         self.assertFalse(Event(0, 20, 0, None) != Event(0, 20, 0, None))
#         self.assertTrue(Event(0, 10, 0, None) != Event(0, 20, 0, None))
#
#     def test_duration(self):
#         self.assertEqual(Event(0, 10, 0, None).duration(), 10)
#         self.assertEqual(Event(20, 100, 0, None).duration(), 80)
#         self.assertEqual(Event(15, 15, 0, None).duration(), 0)


if __name__ == '__main__':
    unittest.main()
