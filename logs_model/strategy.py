
from collections import defaultdict
import random

from logs_model import data
from logs_model.nodes import Node
import logs_model.utils as utils


class AbstractStrategy(object):
    def __init__(self, owner=None):
        self._owner = owner

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, owner):
        self._owner = owner

    def is_server(self) -> bool:
        raise NotImplementedError

    def handle_generate_data_event(self, chunk, curr_time):
        raise NotImplementedError

    def handle_connection_event(self, tm, curr_time):
        raise NotImplementedError

    def handle_disconnection_event(self, tm, curr_time):
        raise NotImplementedError

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        raise NotImplementedError

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        raise NotImplementedError


class AbstractServerStrategy(AbstractStrategy):
    data = defaultdict(lambda: data.MetaData())
    data_amount = defaultdict(int)

    def __init__(self, owner=None):
        super().__init__(owner)

    def is_server(self) -> bool:
        return True

    def __repr__(self):
        res = ""
        for n, mt in sorted(self.data.items(), key=lambda x: x[0].id):
            res += "\t%s(%s): %s, amount = %s\n" % ("S", n.id, mt, self.data_amount[n])
        return res

    @classmethod
    def _store_chunk(cls, chunk, chunk_owner):
        cls.data_amount[chunk_owner] += chunk.amount
        cls.data[chunk_owner].add_chunk(chunk)

    def handle_generate_data_event(self, chunk, curr_time):
        raise NotImplementedError

    def handle_connection_event(self, tm, curr_time):
        raise NotImplementedError

    def handle_disconnection_event(self, tm, curr_time):
        raise NotImplementedError

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        raise NotImplementedError

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        raise NotImplementedError


class SimpleServerStrategy(AbstractServerStrategy):
    def __init__(self, owner=None):
        super().__init__(owner)

    def handle_generate_data_event(self, chunk, curr_time):
        assert False

    def handle_connection_event(self, tm, curr_time):
        return []

    def handle_disconnection_event(self, tm, curr_time):
        return []

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        assert False

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        assert chunk not in AbstractServerStrategy.data[chunk_owner]
        self._store_chunk(chunk, chunk_owner)
        return []


class SmartServerStrategy(AbstractServerStrategy):
    servers = []

    def __init__(self, owner=None):
        super().__init__(owner)

        SmartServerStrategy.servers.append(self)

    def handle_generate_data_event(self, chunk, curr_time):
        assert False

    def handle_connection_event(self, tm, curr_time):
        return []

    def handle_disconnection_event(self, tm, curr_time):
        return []

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        assert False

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        """
        Syncs data between all components connected to server nodes. BFS algorithm used.
        """
        # if False means chunk was committed to server by somebody during transfer
        if chunk not in AbstractServerStrategy.data[chunk_owner]:
            self._store_chunk(chunk, chunk_owner)
            SmartServerStrategy.resync_data(chunk, chunk_owner)

        return []

    @classmethod
    def resync_data(cls, chunk, chunk_owner):
        servs_visited = []
        for serv in cls.servers:
            if serv in servs_visited:
                continue
            servs_visited += cls.dfs(serv.owner.transfer_module, chunk, chunk_owner)

    @staticmethod
    def dfs(root, chunk, chunk_owner):
        server_nodes_visited = []

        for v_tm in utils.dfs(root, []):
            if v_tm.owner.is_server_node():
                server_nodes_visited.append(v_tm.owner.strategy)
            else:
                v_tm.owner.strategy.commit_chunk(chunk, chunk_owner)
        return server_nodes_visited

    def is_metadata_equal(self, md_dict):
        return self.data == md_dict


class SimpleStrategy(AbstractStrategy):
    def __init__(self, owner=None):
        super().__init__(owner)

        self.data = data.LogData()
        # self.data_limit = 100000     # TODO: data_limit

        self.connection_in_use = None

    def __repr__(self):
        return str(self.data)

    def is_server(self) -> bool:
        return False

    def handle_generate_data_event(self, chunk, curr_time):
        self.data.append_data(chunk)
        events = self._start_data_chunk_transfer(curr_time)
        return events

    def handle_connection_event(self, tm, curr_time):
        if self.connection_in_use is not None or type(tm.owner) != Node:
            return []

        self.connection_in_use = tm
        return self._start_data_chunk_transfer(curr_time)

    def handle_disconnection_event(self, tm, curr_time):
        events = []

        if self.connection_in_use == tm:
            self.connection_in_use = None

            # after disconnect try to start transfer with another server node
            for tm_it in self.owner.transfer_module.connections:
                if type(tm_it.owner) == Node and self.owner.transfer_module.connections[tm_it][0]:
                    self.connection_in_use = tm_it

                    events += self._start_data_chunk_transfer(curr_time)
                    break
        return events

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        self.data.delete_chunk(chunk)
        events = self._start_data_chunk_transfer(curr_time)
        return events

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        # just ignore it
        return []

    def _start_data_chunk_transfer(self, curr_time):
        chunk = self.data.get_earliest_chunk()
        if chunk is None:
            return []

        tm = self.connection_in_use
        if tm is None:
            return []

        if self.owner.transfer_module.connections[tm][2]:  # if connection already utilised
            return []

        assert self.owner
        return self.owner.transfer_module.start_data_chunk_transfer(tm, chunk, curr_time, self.owner)


class SmartStrategy(AbstractStrategy):
    def __init__(self, owner=None):
        super().__init__(owner)
        self.nodes_data = defaultdict(lambda: (data.LogData(), data.MetaData()))
        self.server_connection_in_use = None

    def __repr__(self):
        res = ""
        for n, d in sorted(self.nodes_data.items(), key=lambda x: x[0].id):
            dt, mt = d
            res += "\t%s(%s): (%s, %s)\n" % (self.owner.id, n.id, dt, mt)
        return res

    def is_server(self) -> bool:
        return False

    def commit_chunk(self, chunk, chunk_owner):
        self.nodes_data[chunk_owner][1].add_chunk(chunk)
        self.nodes_data[chunk_owner][0].discard_chunk(chunk)

    def handle_generate_data_event(self, chunk, curr_time):
        self.nodes_data[self.owner][0].append_data(chunk)
        return self._start_data_chunk_transfer(curr_time, chunk, self.owner)

    def handle_connection_event(self, tm, curr_time):
        # when two nodes connect, only first should init meta data sync
        # if connection on other end already exist, then meta data was already synced by other node
        if tm.owner.strategy.is_server() or tm.connections[self.owner.transfer_module][0] is False:
            self._sync_meta_data(tm)

        # if using connection to another server, then do not start any other transfers
        if self.server_connection_in_use is not None:
            return []

        # if new connection is to server node then disable all other connections
        # and cancel all other transfers
        if type(tm.owner) == Node:
            for _, conn in self.owner.transfer_module.connections.items():
                conn[2] = False
            self.server_connection_in_use = tm
        return self._start_data_chunk_transfer_to_node(tm, curr_time)

    def handle_disconnection_event(self, tm, curr_time):
        # nothing to do if it was not active connection to server
        if self.server_connection_in_use != tm:
            return []

        # if it was active connection to server, then try to use another server connection
        self.server_connection_in_use = self.owner.transfer_module.get_server_connection()
        if self.server_connection_in_use:
            return self._start_data_chunk_transfer_to_node(self.server_connection_in_use, curr_time)

        # otherwise we are connected only to normal nodes, start transfer to all of them
        events = []
        for tm, conn in self.owner.transfer_module.connections.items():
            if not conn[0]:
                continue
            assert not conn[2]  # normal nodes have not to be used, because we were connected to server
            events += self._start_data_chunk_transfer_to_node(tm, curr_time)
        return events

    def handle_receive_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        node_data = self.nodes_data[chunk_owner]
        if chunk not in node_data[1]:   # if False means chunk was committed to server by somebody during transfer
            if chunk not in node_data[0]:
                node_data[0].append_data(chunk)
                return self._start_data_chunk_transfer(curr_time, chunk, chunk_owner)
            # else:
            # TODO: add it to some statistic maybe?
            #     print("\tNOTE: chunk (%s: %s) was already received!" % (chunk_owner, chunk))
        return []

    def handle_transfer_data_chunk(self, tm, curr_time, chunk, chunk_owner):
        if type(tm.owner) == Node:
            node_data = self.nodes_data[chunk_owner]
            assert chunk in node_data[1]   # chunk should be committed by DFS in SmartServerStrategy

        return self._start_data_chunk_transfer_to_node(tm, curr_time)

    def _choose_data_chunk_to_transfer(self, other_node):
        """
        Chooses what data to transfer to node 'other_node'.

        :param other_node: node where chunk will be transferred
        :return: data chunk to transfer or None if nothing to transfer.
        """
        assert self.owner

        for n, d in self._get_data_by_priority(other_node):
            if type(other_node) == Node:
                chunk = d.get_earliest_chunk()
            else:
                other_data = other_node.strategy.nodes_data[n][0]
                chunk = d.get_earliest_chunk_from_complement(other_data)
            if chunk is not None:
                return n, chunk
        return None, None  # means there is nothing to transfer

    def _get_data_by_priority(self, other_node):
        """
        Returns generator to iterate through nodes_data by priority

        :param other_node: node which data should not be chosen. Could be None
        :return: generator with items: "Node : LogData"
        """
        yield self.owner, self.nodes_data[self.owner][0]
        for n, d in random.sample(self.nodes_data.items(), len(self.nodes_data)):
            if n is not other_node and n is not self:
                yield n, d[0]

    def _start_data_chunk_transfer_to_node(self, tm, curr_time, chunk=None, chunk_owner=None):
        assert self.owner

        if chunk is None:
            chunk_owner, chunk = self._choose_data_chunk_to_transfer(tm.owner)
        else:
            assert chunk_owner
            assert chunk in self.nodes_data[chunk_owner][0]
            if type(tm.owner) != Node:
                assert chunk not in tm.owner.strategy.nodes_data[chunk_owner][0]

        if chunk is None:
            return []
        return self.owner.transfer_module.start_data_chunk_transfer(tm, chunk, curr_time, chunk_owner)

    def _start_data_chunk_transfer(self, curr_time, chunk, chunk_owner):
        """
        Tries to send data chunk via unused connections
        """
        # if connected to server, try to send chunk
        if self.server_connection_in_use is not None:
            tm = self.server_connection_in_use
            if self.owner.transfer_module.connections[tm][2]:
                return []
            e = self._start_data_chunk_transfer_to_node(tm, curr_time, chunk, chunk_owner)
            return e

        # if not connected to server, then try to send to any node which connection is not used
        events = []
        for tm, conn in self.owner.transfer_module.connections.items():
            if not conn[0] or conn[2]:
                continue
            if chunk in tm.owner.strategy.nodes_data[chunk_owner][0]:   # skip if node does not need it
                continue
            events += self._start_data_chunk_transfer_to_node(tm, curr_time, chunk, chunk_owner)
        return events

    def _sync_meta_data(self, tm):
        """
        Sync meta data for new graph connected component.

        :param tm: TransferModule of other connection end
        :return: None
        """
        # get meta_data after sync between syncing nodes
        md_dict = {}
        if tm.owner.is_server_node():
            md_dict = tm.owner.strategy.data
        else:
            union_nodes = set(self.nodes_data.keys()).union(tm.owner.strategy.nodes_data.keys())
            for node in union_nodes:
                m1 = self.nodes_data[node][1]
                m2 = tm.owner.strategy.nodes_data[node][1]
                md_dict[node] = m1 + m2

        # update meta data and log data for whole graph component by self node side
        if not self.is_metadata_equal(md_dict):
            for it_tm in utils.dfs(self.owner.transfer_module, [tm]):
                assert not it_tm.owner.is_server_node()     # server should have the newest metadata
                it_tm.owner.strategy.update_log_data(md_dict)

        # update meta data and log data for whole graph component by other node side
        if not tm.owner.strategy.is_metadata_equal(md_dict):
            for it_tm in utils.dfs(tm, [self.owner.transfer_module]):
                assert not it_tm.owner.is_server_node()     # server should have the newest metadata
                it_tm.owner.strategy.update_log_data(md_dict)

    def is_metadata_equal(self, md_dict):
        for node, md in md_dict.items():
            if self.nodes_data[node][1] != md:
                return False
        return True

    # NOTE: could be optimized by updating not all meta data
    def update_log_data(self, md_dict):
        """
        Updates LogData for node according to MetaData md_dict

        :param md_dict: meta data to sync with
        :return: None
        """
        for node, md in md_dict.items():
            self.nodes_data[node][1].copy_other(md)
            self.nodes_data[node][0].update_committed(md)
