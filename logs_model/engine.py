from queue import PriorityQueue
from collections import defaultdict

from logs_model.utils import log_message
from logs_model.nodes import *
from logs_model.statistics import EventStatisticsAbstract, FinalStatisticsAbstract
from logs_model.strategy import AbstractServerStrategy, SmartServerStrategy


class Engine(object):
    def __init__(self, limit_time, field):
        self.limit_time = limit_time
        self.curr_time = 0
        self.event_queue = PriorityQueue()
        self.field = field

        self.event_stats = []
        self.final_stats = []

        # TODO: fast bugfix, should be fixed normally!
        AbstractServerStrategy.data = defaultdict(lambda: data.MetaData())
        AbstractServerStrategy.data_amount = defaultdict(int)
        SmartServerStrategy.servers = []

        log_message("Created engine")
        log_message("\t%s" % field)
        log_message("\t time limit: %s" % self.limit_time)
        log_message()

    def add_statistic(self, st):
        if isinstance(st, EventStatisticsAbstract):
            self.event_stats.append(st)
        if isinstance(st, FinalStatisticsAbstract):
            self.final_stats.append(st)

    def add_node(self, node, pos):
        """
        Adds node to field

        :param node: node to be added
        :param pos: pos of node at field
        :return: False - if error occurred, otherwise True
        """
        events = self.field.add_node(node, pos, self.curr_time)
        assert events is not None

        for e in events:
            self.event_queue.put(e)

        log_message("%d: Added N%d to (%d,%d)" % (self.curr_time, node.id, node.prev_pos.x, node.prev_pos.y))
        log_message("\t%s" % node)

    def run(self):
        """
        Starts event-driven modeling.
        Pops queue and calls event handlers

        :return: None
        """
        stat_failed = False
        while not self.event_queue.empty() and self.curr_time < self.limit_time and not stat_failed:
            event = self.event_queue.get()
            self.curr_time = event.end_time

            for e in event.handle(self.field):
                self.event_queue.put(e)

            for st in self.event_stats:
                # if any statistic fails - stop simulation
                stat_failed = st.register_event(event) or stat_failed

        self._finish_handle()

    def _finish_handle(self):
        log_message()
        log_message("Simulation finished!")
        log_message("Nodes:")
        for _, node in sorted(self.field.nodes.items()):
            if not isinstance(node, MovingNode):
                continue
            log_message("N%d's data:\n%s" % (node.id, node.strategy))

        log_message()
        log_message("Broken nodes:")
        for _, node in sorted(self.field.broken_nodes.items()):
            if not isinstance(node, MovingNode):
                continue
            log_message("N%d's data:\n%s" % (node.id, node.strategy))

        log_message()
        log_message("Server data:\n%s" % AbstractServerStrategy())

        log_message()
        log_message("Final statistics: ")
        for st in self.final_stats:
            st.analyse(self.field)
            log_message(str(st))
            log_message()

        log_message()
        log_message("Event based statistics: ")
        for st in self.event_stats:
            st.finish(self.field, self.curr_time)
            log_message(str(st))
            log_message()
