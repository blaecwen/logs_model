#!/usr/bin/python3

import shutil
import gzip
import csv
import sys
from datetime import datetime

import logs_model.field
import logs_model.mathutils
import logs_model.utils

from logs_model import engine
from logs_model.statistics import *


log_filepath = "logs/"
exp_id = 0


class NodeFabric(object):
    def __init__(self, speed, move_radius, strategy="simple"):
        self.speed = speed
        self.move_radius = move_radius
        self.strategy = strategy

        self.break_probability = 0.001

    def create_moving_node(self):
        node = MovingNode(self.speed, self.move_radius, self.strategy)
        node.break_probability = self.break_probability
        return node

    def create_server_node(self):
        return Node(self.strategy)


def run_model(res_filename, exp_id, field, nfb, nodes, cluster: bool):
    e = engine.Engine(float("inf"), field)

    s_converge = DataConvergenceStat()
    s_redundancy = ChunkRedundancyStat()
    s_break = BreakNodeStat()
    s_link = LinkTimeStat()
    s_slink = ServerLinkTimeStat()

    e.add_statistic(s_converge)
    e.add_statistic(s_redundancy)
    e.add_statistic(s_break)
    e.add_statistic(s_link)
    e.add_statistic(s_slink)

    p1 = mathutils.Point(50, 50)
    p2 = field.pick_random_point()

    e.add_node(nfb.create_server_node(), p1)

    for i in range(nodes):
        if cluster:
            e.add_node(nfb.create_moving_node(), p2)
        else:
            e.add_node(nfb.create_moving_node(), field.pick_random_point())
    e.run()

    # log results
    with open(res_filename, 'a', newline='') as csv_file:
        values = [exp_id,
                  e.curr_time, len(field.broken_nodes),
                  s_converge.total,
                  s_redundancy.get_redundancy(),
                  s_break.ttpl,
                  s_link.total_mean,
                  s_slink.mean_server_connection_percentage
                  ]
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(values)


def run_experiments(runtime_log_filename):
    global exp_id
    nfb = NodeFabric(1, 30, "simple")
    nfb.strategy = "smart"
    nfb.speed = 1
    nfb.move_radius = 30
    nfb.break_probability = 0.0005

    prev_time = datetime.now()
    for i in range(1):
        field = logs_model.field.Field(logs_model.mathutils.Point(0, 0), 100, 100)
        cluster = False
        nodes = random.randint(5, 10)

        # log starting parameters
        with open(runtime_log_filename + "_in.csv", 'a', newline='') as csv_file:
            values = [exp_id,
                      1,        # number of server nodes
                      nodes,    # number of nodes
                      field.corner.x, field.corner.y, field.width, field.height,
                      nfb.strategy, nfb.speed, nfb.move_radius, nfb.break_probability,
                      cluster
                      ]
            csv_writer = csv.writer(csv_file, delimiter=',')
            csv_writer.writerow(values)

        # start modeling
        with open(runtime_log_filename, "w") as fl:
            logs_model.utils.log_file = fl
            run_model(runtime_log_filename + "_res.csv", exp_id, field, nfb, nodes, cluster)

        # gzip full modeling log
        with open(runtime_log_filename, 'rb') as f_in:
            with gzip.open(runtime_log_filename + "." + str(exp_id) + ".gz", 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        now = datetime.now()
        print("Model #%s finished at: %s. Run time: %s" % (exp_id, now, now - prev_time))
        prev_time = now
        exp_id += 1


def main():
    # random.seed(77)

    if len(sys.argv) < 2:
        print("Error, first arg should log_filename")
        exit(1)
    instance_name = sys.argv[1]
    runtime_log_filename = log_filepath + instance_name

    print("Start modeling at: %s" % datetime.now())
    while exp_id < 100:
        run_experiments(runtime_log_filename)


if __name__ == "__main__":
    main()
