
from logs_model.events import *
from logs_model.strategy import *
from logs_model.data import *


class EventStatisticsAbstract(object):
    def register_event(self, event):
        """
        Handles events

        :return: True, if failed
        """
        raise NotImplementedError

    def finish(self, field, curr_time):
        raise NotImplementedError


class ChunkRedundancyStat(EventStatisticsAbstract):
    def __init__(self):
        self.chunks = defaultdict(int)
        self.redundancy = None

    def register_event(self, event):
        if type(event) != ChunkTransferEvent:
            return False

        coid = event.chunk_owner.id
        cid = event.chunk.id
        self.chunks[coid, cid] += 1
        return False

    def finish(self, field, curr_time):
        self.redundancy = self.get_redundancy()

    def __repr__(self):
        assert self.redundancy is not None
        name = "ChunkRedundancy"
        return name + ": redundancy = " + str(self.redundancy)

    def get_redundancy(self):
        chunk_num = len(self.chunks)
        if chunk_num == 0:
            return 1
        res = 0
        for _, count in self.chunks.items():
            res += count
        return res / chunk_num


class LinkTimeStat(EventStatisticsAbstract):
    def __init__(self):
        # total times of connections between nodes
        self.connection_times = defaultdict(float)
        # time of connection start between nodes, None if disconnected
        self.connection_start_time = defaultdict(lambda: None)

        # matrix of connection percentages
        self.connection_matrix = defaultdict(dict)

        # total mean time of all connections between all nodes
        self.total_mean = 0

    def register_event(self, event):
        if type(event) != TransferStartEvent and type(event) != TransferEndEvent:
            return False

        # order nodes, to not distinguish such keys: (n1, n2) and (n2, n1)
        n1 = max(event.tm1.owner, event.tm2.owner, key=lambda x: x.id)
        n2 = min(event.tm1.owner, event.tm2.owner, key=lambda x: x.id)

        if type(event) == TransferStartEvent:
            assert self.connection_start_time[n1.id, n2.id] is None
            self.connection_start_time[n1.id, n2.id] = event.end_time
            return False

        if type(event) == TransferEndEvent:
            assert self.connection_start_time[n1.id, n2.id] is not None
            self.connection_times[n1.id, n2.id] += event.end_time - self.connection_start_time[n1.id, n2.id]
            self.connection_start_time[n1.id, n2.id] = None
            return False
        assert False

    def finish(self, field, curr_time):
        self._init_matrix(field)

        nodes = dict(field.nodes)
        nodes.update(field.broken_nodes)

        for pair, start_time in self.connection_start_time.items():
            id1, id2 = pair
            if start_time is not None:
                self.connection_times[id1, id2] += curr_time - start_time

            # fill matrix
            conn_time = self.connection_times[id1, id2]
            total_time = min(field.break_node_times.get(id1, curr_time), field.break_node_times.get(id2, curr_time))
            self.connection_matrix[id1][id2] = conn_time / total_time * 100
            self.connection_matrix[id2][id1] = conn_time / total_time * 100

            # total_mean
            self.total_mean += self.connection_matrix[id1][id2]
        self.total_mean /= (len(nodes)*(len(nodes)-1)) / 2

    def __repr__(self):
        res = "LinkTimeStat:\n"
        for pair, time in self.connection_times.items():
            id1, id2 = pair
            res += "(N%s, N%s): %s\n" % (id1, id2, time)
        res += "matrix:\n" + self.get_str_matrix()
        res += "\n" + "total: %s" % self.total_mean
        return res

    def _init_matrix(self, field):
        for k1 in (list(field.nodes.keys()) + list(field.broken_nodes.keys())):
            for k2 in (list(field.nodes.keys()) + list(field.broken_nodes.keys())):
                self.connection_matrix[k1][k2] = 0
                self.connection_matrix[k2][k1] = 0

    def get_str_matrix(self):
        res = "  "
        for key in self.connection_matrix.keys():
            res += '{:6}'.format(key) + ' '
        res += "\n"
        for k1, row in self.connection_matrix.items():
            res += '{:3}'.format(k1) + '  '
            for k2, val in row.items():
                res += '{:6}'.format("%.2f" % val) + ' '
            res += "\n"
        return res


class ServerLinkTimeStat(EventStatisticsAbstract):
    def __init__(self):
        # server connection time for every node
        self.server_connection_times = defaultdict(float)
        # time of connection start between node and server for every node, None if disconnected
        self.server_connection_start_time = defaultdict(lambda: None)

        # server connection time for every node in percentages (time / total time)
        self.server_connection_percentages = dict()

        self.mean_server_connection_percentage = 0

    def register_event(self, event):
        if type(event) != TransferStartEvent and type(event) != TransferEndEvent:
            return False

        # order nodes, to not distinguish such keys: (n1, n2) and (n2, n1)
        n1 = max(event.tm1.owner, event.tm2.owner, key=lambda x: x.id)
        n2 = min(event.tm1.owner, event.tm2.owner, key=lambda x: x.id)

        if type(event) == TransferStartEvent:
            # register connection to server
            if n1.is_server_node() and self.server_connection_start_time[n2.id] is None:
                self.server_connection_start_time[n2.id] = event.end_time
            if n2.is_server_node() and self.server_connection_start_time[n1.id] is None:
                self.server_connection_start_time[n1.id] = event.end_time
            return False

        if type(event) == TransferEndEvent:
            # if disconnected from all servers, add to server connection times
            if n1.is_server_node() and n2.transfer_module.get_server_connection() is None:
                assert self.server_connection_start_time[n2.id] is not None
                self.server_connection_times[n2.id] += event.end_time - self.server_connection_start_time[n2.id]
                self.server_connection_start_time[n2.id] = None

            if n2.is_server_node() and n1.transfer_module.get_server_connection() is None:
                assert self.server_connection_start_time[n1.id] is not None
                self.server_connection_times[n1.id] += event.end_time - self.server_connection_start_time[n1.id]
                self.server_connection_start_time[n1.id] = None
            return False
        assert False

    def finish(self, field, curr_time):
        nodes = dict(field.nodes)
        nodes.update(field.broken_nodes)

        for nid in nodes.keys():
            tmp = self.server_connection_times[nid]     # just to init all nodes
            self.server_connection_percentages[nid] = 0
            if nodes[nid].is_server_node():
                self.server_connection_times[nid] = curr_time

        for nid, start_time in self.server_connection_start_time.items():
            if nodes[nid].is_server_node():
                continue
            if start_time is None:
                continue
            self.server_connection_times[nid] += curr_time - start_time

        for nid, time in self.server_connection_times.items():
                self.server_connection_percentages[nid] = time / field.break_node_times.get(nid, curr_time) * 100

        self._calc_total_mean(field)

    def __repr__(self):
        res = "ServerLinkTimeStat:\n"
        for n, time in sorted(self.server_connection_percentages.items(), key=lambda x: x[0]):
            res += "N%s: %s (%s)\n" % (n, time, self.server_connection_times[n])
        res += "Mean server connection percent: %s" % self.mean_server_connection_percentage
        return res

    def _calc_total_mean(self, field):
        nodes = dict(field.nodes)
        nodes.update(field.broken_nodes)

        sum = 0
        count = 0
        for nid, node in nodes.items():
            if node.is_server_node():
                continue
            count += 1
            sum += self.server_connection_percentages[nid]
        self.mean_server_connection_percentage = sum / count


class BreakNodeStat(EventStatisticsAbstract):
    def __init__(self):
        self.ttpl = float("inf")

    def register_event(self, event):
        if type(event) != NodeBreakEvent:
            return False

        if type(event.node.strategy) == SimpleStrategy:
            if len(event.node.strategy.data._data) > 1:  # we can lose one just generated packet
                self.ttpl = event.end_time
                return True
            return False

        # SmartStrategy here
        nodes = list(event.field.nodes.values())
        assert event.node not in nodes

        # at first search nodes own chunks
        cnt = 0
        for chunk in event.node.strategy.nodes_data[event.node][0]._data:
            if not self._is_chunk_stored_at_nodes(nodes, chunk, event.node):
                cnt += 1
                if cnt > 1:     # we can lose one just generated packet
                    self.ttpl = event.end_time
                    return True

        # now search other chunks
        for n, dt in event.node.strategy.nodes_data.items():
            ld, md = dt
            if n == event.node:
                continue
            if n in nodes:      # if node is in field, we could not lose it's data
                continue
            for chunk in ld._data:
                if not self._is_chunk_stored_at_nodes(nodes, chunk, n):
                    self.ttpl = event.end_time
                    return True
        return False

    @staticmethod
    def _is_chunk_stored_at_nodes(nodes, chunk, chunk_owner):
        if chunk in SmartServerStrategy.data[chunk_owner]:
            return True

        for node in nodes:
            if node.is_server_node():
                continue
            if chunk in node.strategy.nodes_data[chunk_owner][0]:
                return True
        return False

    def finish(self, field, curr_time):
        pass    # TODO: move here some logic

    def __repr__(self):
        name = "BreakNodeStat"
        return name + ": TTPL = " + str(self.ttpl)


class FinalStatisticsAbstract(object):
    def analyse(self, field):
        raise NotImplementedError


class DataConvergenceStat(FinalStatisticsAbstract):
    def __init__(self):
        self.converge_values = dict()
        self.total = 100

    def analyse(self, field):
        self.converge_values = dict()
        for node in (list(field.nodes.values()) + list(field.broken_nodes.values())):
            if node.is_server_node():
                continue

            if type(node.strategy) == SmartStrategy:
                md = AbstractServerStrategy.data[node]
                ld = LogData()
                ld.copy_other(node.strategy.nodes_data[node][0])
                ld.update_committed(md)
            else:
                ld = node.strategy.data

            s_amount = AbstractServerStrategy.data_amount[node]
            if s_amount + ld.total == 0:
                self.converge_values[node] = 100
            else:
                self.converge_values[node] = s_amount / (ld.total + s_amount) * 100

    def __repr__(self):
        res = "DataConvergenceStat:\n"
        for n, v in sorted(self.converge_values.items(), key=lambda x: x[0].id):
            res += "\t%d: %s\n" % (n.id, v)
        self.total = sum(self.converge_values.values()) / len(self.converge_values)
        res += "\ttotal: %s" % self.total
        return res
