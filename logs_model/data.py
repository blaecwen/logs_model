import random
from blist import sortedset


class DataGenerator(object):
    def __init__(self, period, period_delta, amount, amount_delta):
        assert period >= period_delta
        assert amount > amount_delta

        self.period = period
        self.period_delta = period_delta
        self.amount = amount
        self.amount_delta = amount_delta
        self.id_counter = 0

    def generate_data_chunk(self, time):
        delta = random.uniform(-self.amount_delta, self.amount_delta)
        res = LogDataEntry(self.amount + delta, time, self.id_counter)
        self.id_counter += 1
        return res

    def get_next_time(self):
        return self.period + random.uniform(-self.period_delta, self.period_delta)

    def __repr__(self):
        return "".join(["<T:", self.period, "+-", self.period_delta,
                        ", Q:", self.amount, "+-", self.amount_delta, ">"])


class MetaData(object):
    """
    MetaData - is a data about which LogDataEntries was committed to server and should not be stored anymore
    """
    def __init__(self, horizon_data_id=0, id_list=list()):
        """
        Creates MetaData object

        :param horizon_data_id: data chunk id up to which all data was committed, excluding the chunk
        """
        self.horizon_data_id = horizon_data_id
        self.data = sortedset(id_list)         # id's of data entries after horizon
        self._update_horizon()

    # TODO: test it
    def __add__(self, other):
        """
        Union of data from self and other
        :return: new MetaData object
        """
        new_horizon = max(self.horizon_data_id, other.horizon_data_id)
        new_data = sortedset((d for d in self.data.union(other.data) if d >= new_horizon))

        new_md = MetaData(new_horizon)
        new_md.data = new_data
        new_md._update_horizon()
        return new_md

    # TODO: test it
    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def __contains__(self, item: 'LogDataEntry') -> bool:
        if item.id < self.horizon_data_id:
            return True
        return item.id in self.data

    def __repr__(self):
        if len(self.data) == 0:
            return "<h:%s, min:%s, max:%s, len:%s>" % (self.horizon_data_id, None, None, len(self.data))
        return "<h:%s, min:%s, max:%s, len:%s>" % (self.horizon_data_id, self.data[0], self.data[-1], len(self.data))

    def add_chunk(self, chunk):
        assert chunk.id >= self.horizon_data_id
        assert chunk.id not in self.data

        self.data.add(chunk.id)
        self._update_horizon()
        return True

    def _update_horizon(self):
        while self.data and self.horizon_data_id == self.data[0]:
            self.horizon_data_id += 1
            self.data.pop(0)

    def copy_other(self, other):
        self.horizon_data_id = other.horizon_data_id
        self.data = other.data.copy()

    def sync_meta(self, other):
        """
        Synchronises two metaData objects

        :param other: other metaData object
        """
        new_horizon = max(self.horizon_data_id, other.horizon_data_id)
        new_data = sortedset((d for d in self.data.union(other.data) if d >= new_horizon))

        self.horizon_data_id = new_horizon
        self.data = new_data
        self._update_horizon()

        other.horizon_data_id = self.horizon_data_id
        other.data = new_data.copy()


class LogDataEntry(object):
    def __init__(self, amount, time, entry_id, int_repr=True):
        assert amount >= 0
        assert time >= 0
        self.amount = amount
        self.time = time
        self.id = entry_id

        self.int_repr = int_repr

    def __eq__(self, other: 'LogDataEntry') -> bool:
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        if self.int_repr:
            format_str = "<id:%d, t1:%d, Q:%d>"
        else:
            format_str = "<id:%s, t1:%s, Q:%s>"
        return format_str % (self.id, self.time, self.amount)


# TODO: assert if id already exist
# TODO: delete time_end
# TODO: delete time_start
class LogData(object):
    def __init__(self, int_repr=True):
        self._data = sortedset(key=lambda x: x.id)
        self.time_start = float("inf")
        self.time_end = 0
        self.total = 0
        self.int_repr = int_repr

    def __repr__(self):
        if self.int_repr:
            format_str = "<t0:%d, t1:%d, Q:%d, len:%d>"
            if self.time_start == float("inf"):
                format_str = "<t0:%s, t1:%d, Q:%d, len:%d>"
        else:
            format_str = "<t0:%s, t1:%s, Q:%s, len:%d>"

        return format_str % (self.time_start, self.time_end, self.total, len(self._data))

    # def __iadd__(self, other):
    #     self.total += other.total
    #     self.time_end = max(self.time_end, other.time_end)
    #     self.time_start = min(self.time_start, other.time_start)
    #
    #     self._data += other._data
    #     self._data.sort(key=lambda x: x.time)
    #     return self

    def __contains__(self, item: LogDataEntry) -> bool:
        return item in self._data

    def get_earliest_chunk(self) -> LogDataEntry:
        if self.total == 0:
            return None
        return self._data[0]

    def get_earliest_chunk_from_complement(self, other_data: 'LogData') -> LogDataEntry:
        """
        Return the earliest chunk from complement with data
        NOTE: could be optimised if use the fact that data is sorted

        :param other_data: another data to complement with
        :return: data chunk
        """

        complement = (x for x in self._data if x not in other_data._data)
        return next(complement, None)

    def get_earliest_chunk_meta(self, meta: MetaData) -> LogDataEntry:
        """
        Return the earliest chunk with respect to metaData
        NOTE: could be optimised if use the fact that data is sorted

        :param meta: MetaData
        :return: data chunk
        """
        uncommitted_data = (x for x in self._data if x.id >= meta.horizon_data_id and x.id not in meta.data)
        return next(uncommitted_data, None)

    # TODO: test it!!
    def discard_chunk(self, chunk):
        len_before = len(self._data)
        self._data.discard(chunk)
        len_after = len(self._data)

        # means chunk was not deleted
        if len_before == len_after:
            return

        if len_after == 0:
            self.clear()
            return

        self.total -= chunk.amount
        self.time_start = self._data[0].time
        self.time_end = self._data[-1].time

    def delete_chunk(self, chunk):
        assert len(self._data) != 0

        if len(self._data) == 1:
            assert self._data[0] == chunk
            self.clear()
            return

        self._data.remove(chunk)
        self.total -= chunk.amount
        self.time_start = self._data[0].time
        self.time_end = self._data[-1].time

    # TODO: should I copy data chunk?
    def append_data(self, data):
        if data.amount == 0:
            return
        assert data not in self     # NOTE: disable it to optimise program!

        self.time_start = min(data.time, self.time_start)
        self.time_end = max(data.time, self.time_end)
        self.total += data.amount
        self._data.add(data)

    def update_committed(self, meta: MetaData):
        """
        Updates data according to MetaData by deleting entries stored in meta from self._data
        """
        self._data = sortedset((d for d in self._data if d.id >= meta.horizon_data_id and d.id not in meta.data),
                               key=lambda x: x.id)

        if len(self._data) == 0:
            self.clear()
            return

        self.total = sum(d.amount for d in self._data)
        self.time_end = self._data[-1].time
        self.time_start = self._data[0].time

    def copy_other(self, other):
        self._data = other._data.copy()
        self.time_start = other.time_start
        self.time_end = other.time_end
        self.total = other.total
        self.int_repr = other.int_repr

    def clear(self):
        self._data = sortedset(key=lambda x: x.id)
        self.time_start = float("inf")
        self.time_end = 0
        self.total = 0
