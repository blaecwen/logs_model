setwd("~/workspace/pythonProgramms/logs_model/logs/analysis")

read_data <- function(name, files_num) {
  next_id <- 0
  data <- data.frame()
  for (i in 1:files_num) {
    n <- sprintf(name, i)
    df_data <- read.csv(n, header = FALSE)
    df_data$V1 <- df_data$V1 + nrow(data)
    data <- rbind(data, df_data)
  }
  
  return(data)
}

input_names <- function() {
  return(c("id", "snodes", "nodes",
           "f.x", "f.y", "f.width", "f.height",
           "nfb.strategy", "nfb.speed", "nfb.mradius", "nfb.break_prob","cluster"
  ))
}

output_names <- function() {
  return(c("id",
           "duration", "broken",
           "converge", "redundancy", "ttpl",
           "link_mean", "slink_mean"))
}

load_data <- function(in.filename, out.filename, num) {
  input <- read_data(in.filename, num)
  output <- read_data(out.filename, num)
  names(input) <- input_names()
  names(output) <- output_names()
  
  # combine in and out data
  data <- merge(input, output, by="id")
  data$f.area <- data$f.width * data$f.height
  return(data)
}

load_field_300_simple <- function() {
  input <- read_data("field_300_simple/h%s_zorz1_in.csv", 3)
  output <- read_data("field_300_simple/h%s_zorz1_res.csv", 3)
  names(input) <- input_names()
  names(output) <- output_names()
  
  # combine in and out data
  data <- merge(input, output, by="id")
  data$f.area <- data$f.width * data$f.height
  return(data)
}

load_field_300_smart <- function() {
  w.input <- data.frame()
  w.output <- data.frame()
  
  input <- read_data("field_300_smart_2/w%s_exp1_in.csv", 3)
  output <- read_data("field_300_smart_2/w%s_exp1_res.csv", 3)
  
  input$V1 <- input$V1 + nrow(w.input)
  w.input <- rbind(w.input, input)
  output$V1 <- output$V1 + nrow(w.output)
  w.output <- rbind(w.output, output)
  
  input <- read_data("field_300_smart_1/h%s_zorz1_in.csv", 2)
  output <- read_data("field_300_smart_1/h%s_zorz1_res.csv", 2)
  
  input$V1 <- input$V1 + nrow(w.input)
  w.input <- rbind(w.input, input)
  output$V1 <- output$V1 + nrow(w.output)
  w.output <- rbind(w.output, output)
  
  input <- read_data("field_300_smart_3/s%s_exp1_in.csv", 2)
  output <- read_data("field_300_smart_3/s%s_exp1_res.csv", 2)
  
  input$V1 <- input$V1 + nrow(w.input)
  w.input <- rbind(w.input, input)
  output$V1 <- output$V1 + nrow(w.output)
  w.output <- rbind(w.output, output)
  
  names(w.input) <- input_names()
  names(w.output) <- output_names()
  
  w.data <- merge(w.input, w.output, by="id")
  w.data$f.area <- w.data$f.width * w.data$f.height
  return(w.data)
}

load_cluster_simple <- function() {
  data <- load_data("cluster_simple/h%s_zorz1_in.csv", "cluster_simple/h%s_zorz1_res.csv", 3)
  return(data)
}

load_cluster_smart <- function() {
  w.data <- data.frame()
  w.data <- rbind(w.data, load_data("cluster_smart_1/h%s_zorz1_in.csv",
                                    "cluster_smart_1/h%s_zorz1_res.csv", 3))
  w.data <- rbind(w.data, load_data("cluster_smart_2/w%s_exp2_in.csv",
                                    "cluster_smart_2/w%s_exp2_res.csv", 3))
  w.data <- rbind(w.data, load_data("cluster_smart_3/s%s_exp2_in.csv",
                                    "cluster_smart_3/s%s_exp2_res.csv", 2))
  return(w.data)
}

load_exp3_simple <- function() {
  data <- load_data("exp3_simple/h%s_exp3_in.csv",
                    "exp3_simple/h%s_exp3_res.csv", 3)
  return(data)
}

load_exp3_smart <- function() {
  w.data <- data.frame()
  w.data <- rbind(w.data, load_data("exp3_smart_1/h%s_exp3_in.csv",
                                    "exp3_smart_1/h%s_exp3_res.csv", 3))
  w.data <- rbind(w.data, load_data("exp3_smart_2/w%s_exp3_in.csv",
                                    "exp3_smart_2/w%s_exp3_res.csv", 3))
  w.data <- rbind(w.data, load_data("exp3_smart_3/s%s_exp3_in.csv",
                                    "exp3_smart_3/s%s_exp3_res.csv", 2))
  return(w.data)
}

load_exp4_smart <- function() {
  w.data <- data.frame()
  w.data <- rbind(w.data, load_data("exp4_smart_2/h%s_exp4_in.csv",
                                    "exp4_smart_2/h%s_exp4_res.csv", 3))
  w.data <- rbind(w.data, load_data("exp4_smart_1/w%s_exp4_in.csv",
                                    "exp4_smart_1/w%s_exp4_res.csv", 3))
  w.data <- rbind(w.data, load_data("exp4_smart_3/s%s_exp4_in.csv",
                                    "exp4_smart_3/s%s_exp4_res.csv", 2))
  return(w.data)
}

load_exp4_simple <- function() {
  data <- load_data("exp4_simple/w%s_exp4_in.csv",
                    "exp4_simple/w%s_exp4_res.csv", 2)
  return(data)
}

data.e4 <- load_exp4_simple()
w.data.e4 <- load_exp4_smart()

data.e3 <- load_exp3_simple()
w.data.e3 <- load_exp3_smart()

data.e2 <- load_cluster_simple()
w.data.e2 <- load_cluster_smart()

data1k <- load_data("field_1k_simple/h%s_zorz1_in.csv", "field_1k_simple/h%s_zorz1_res.csv", 3)
w.data1k <- load_data("field_1k_smart/w%s_exp1_in.csv", "field_1k_smart/w%s_exp1_res.csv", 3)
data1k$cluster <- "True"
w.data1k$cluster <- "True"

data300 <- load_field_300_simple()
w.data300 <- load_field_300_smart()

  
#=====================================================#
# analyse
library(ggplot2)
plot_nice <- function(gdata, aes_param, aes_color) {
  ggplot(gdata, aes_param) +
    geom_point(aes_color, position="jitter", alpha=0.5) +
    geom_boxplot(alpha=0.1) 
    #scale_x_discrete(breaks = round(seq(min(data$f.width), max(data$f.width), by = 10),1)) 
    #scale_y_discrete(breaks = round(seq(0, 100, by = 1),1))
}

sdata.e4 <- subset(data.e4, duration > 1000)
sdata.e3 <- subset(data.e3, duration > 1000)
sdata.e2 <- subset(data.e2, duration > 1000)
sdata1k <- subset(data1k, duration > 1000)
sdata300 <- subset(data300, duration > 1000)
sdata_cluster <- subset(sdata.e2, cluster == "True")
sdata_ncluster <- subset(sdata.e2, cluster == "False")


w.sdata.e4 <- subset(w.data.e4, duration > 1000)
w.sdata.e3 <- subset(w.data.e3, duration > 1000)
w.sdata.e2 <- subset(w.data.e2, duration > 1000)
w.sdata1k <- subset(w.data1k, duration > 1000)
w.sdata300 <- subset(w.data300, duration > 1000)
w.sdata_cluster <- subset(w.sdata.e2, cluster == "True")
w.sdata_ncluster <- subset(w.sdata.e2, cluster == "False")

tmp <- subset(data300, duration > 1000 & f.width == 100 | f.width == 200 | f.width == 300)
tmp <- rbind(data1k, tmp)
ggplot(tmp, aes(as.factor(f.width), converge)) +
  geom_point(position="jitter", alpha=0.5) +
  geom_boxplot(alpha=0.1) +
  theme_bw() +
  labs(x="Field size", y="Convergence, %")

tmp <- subset(w.data300, duration > 1000 & f.width == 100 | f.width == 200 | f.width == 300)
tmp <- rbind(w.data1k, tmp)
ggplot(tmp, aes(as.factor(f.width), converge)) +
  geom_point(position="jitter", alpha=0.5) +
  geom_boxplot(alpha=0.1) +
  theme_bw() +
  labs(x="Field size", y="Convergence, %")

plot_nice(sdata.e2, aes(as.factor(f.width), converge), aes(colour=factor(f.width)))
plot_nice(w.sdata.e2, aes(as.factor(f.width), converge), aes(colour=factor(f.width)))

plot_nice(sdata300, aes(as.factor(f.width), broken), aes(colour=factor(f.width)))
plot_nice(w.sdata300, aes(as.factor(f.width), broken), aes(colour=factor(f.width)))

tmp <- rbind(sdata_cluster, w.sdata_cluster)
plot_nice(tmp, aes(as.factor(nfb.strategy), broken), aes(colour=factor(nfb.strategy)))

plot_nice(data.e3, aes(as.factor(nodes), converge), aes(colour=factor(nodes)))
plot_nice(w.data.e3, aes(as.factor(nodes), converge), aes(colour=factor(nodes)))


plot_nice(rbind(sdata_cluster, w.sdata_cluster),
          aes(as.factor(f.width), link_mean), aes(colour=factor(f.width)))
plot_nice(rbind(sdata_ncluster, w.sdata_ncluster),
          aes(as.factor(f.width), slink_mean), aes(colour=factor(f.width)))

tmp <- subset(w.sdata1k, f.width < 300)
tmp$cluster <- "True"
tmp <- rbind(w.data300, tmp)
tmp <- subset(tmp, ttpl < 30000)
plot_nice(tmp, aes(as.factor(f.width), ttpl), aes(colour=factor(f.width)))

tmp <- subset(sdata1k, f.width < 300)
tmp$cluster <- "True"
tmp <- rbind(data300, tmp)
tmp <- subset(tmp, ttpl < 30000)
plot_nice(tmp, aes(as.factor(f.width), ttpl), aes(colour=factor(f.width)))


ggplot(subset(w.sdata, link_mean < 16), aes(link_mean, converge)) + geom_point(alpha=0.5) + geom_smooth() +
  scale_y_continuous(breaks = round(seq(0, 100, by = 10),1))
ggplot(subset(sdata, link_mean < 16), aes(link_mean, converge)) + geom_point(alpha=0.5) + geom_smooth() +
  scale_y_continuous(breaks = round(seq(0, 100, by = 10),1))

# extract values
converge_data <- sdata[, c("id","duration","converge")]

data_zero_conv <- subset(converge_data, converge == 0)
data_zero_conv <- data_zero_conv[order(data_zero_conv$duration),]
tail(data_zero_conv)

##########################################
# 1 experiment
# Convergence depending on the field size
tmp <- subset(sdata1k, f.width < 300)
tmp <- rbind(sdata300, tmp)
tmp <- rbind(subset(sdata_cluster), tmp)
ggplot(tmp, aes(as.factor(f.width), converge)) +
  geom_jitter(alpha=0.3, height = 0, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  labs(x="Field size", y="Convergence, %")

tmp <- subset(w.sdata1k, f.width < 300)
tmp <- rbind(w.data300, tmp)
tmp <- rbind(subset(w.sdata_cluster), tmp)
ggplot(tmp, aes(as.factor(f.width), converge)) +
  geom_jitter(alpha=0.3, height = 0, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  labs(x="Field size", y="Convergence, %")


##########################################
# 3 experiment
# Time to system failure depending on number of mobile nodes
ggplot(data.e3, aes(as.factor(nodes), duration)) +
  geom_jitter(alpha=0.3, height = 0, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  coord_cartesian(xlim = c(0, 7), ylim = c(0, 50000), expand = FALSE) +
  labs(x="Mobile nodes number", y="Time to system failure")

ggplot(w.data.e3, aes(as.factor(nodes), duration)) +
  geom_jitter(alpha=0.3, height = 0, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  coord_cartesian(xlim = c(0, 7), ylim = c(0, 50000), expand = FALSE) +
  labs(x="Mobile nodes number", y="Time to system failure")


##########################################
# 4 experiment
# Time to system failure depending on number of mobile nodes
ggplot(data.e4, aes(as.factor(nodes), duration)) +
  geom_jitter(alpha=0.3, height = 0, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  coord_cartesian(xlim = c(0, 7), ylim = c(0, 50000), expand = FALSE) +
  labs(x="Mobile nodes number", y="Time to system failure")

ggplot(w.data.e4, aes(as.factor(nodes), duration)) +
  geom_jitter(alpha=0.3, height = 0, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  coord_cartesian(xlim = c(0, 7), ylim = c(0, 50000), expand = FALSE) +
  labs(x="Mobile nodes number", y="Time to system failure")

# Number of mobile nodes broken during simulation depending on number of mobile nodes
ggplot(data.e4, aes(as.factor(nodes), broken)) +
  geom_jitter(alpha=0.3, height = 0.3, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  scale_y_discrete(limits = seq(1, 10, by=1)) +
  coord_cartesian(ylim = c(1, 10), expand = TRUE) +
  labs(x="Mobile nodes number", y="Mobile nodes broken") 

ggplot(w.data.e4, aes(as.factor(nodes), broken)) +
  geom_jitter(alpha=0.3, height = 0.3, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  scale_y_discrete(limits = seq(1, 10, by=1)) +
  labs(x="Mobile nodes number", y="Mobile nodes broken") 

# Redundancy of system depending on number of mobile nodes
ggplot(w.data.e4, aes(as.factor(nodes), redundancy)) +
  geom_jitter(alpha=0.3, height = 0.3, width = 0.25) +
  geom_boxplot(alpha=0.2) +
  theme_bw() +
  coord_cartesian(ylim = c(1, 5), expand = TRUE) +
  scale_y_continuous(breaks = seq(1, 5, by=1)) +
  labs(x="Mobile nodes number", y="Redundancy") 


